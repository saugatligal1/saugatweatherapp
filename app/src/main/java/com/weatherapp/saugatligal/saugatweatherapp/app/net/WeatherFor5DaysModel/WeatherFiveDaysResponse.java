package com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel;

import java.util.ArrayList;
import java.util.List;

public class WeatherFiveDaysResponse {

    private City city;
    private List<com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.List> list;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.List> getList() {
        return list;
    }

    public void setList(List<com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.List> list) {
        this.list = list;
    }
}
