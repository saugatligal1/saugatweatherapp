package com.weatherapp.saugatligal.saugatweatherapp.app.services;

import android.support.annotation.NonNull;

public interface WeatherApiService {

    interface Listener<T> {
        void onData(T data);

        void onError();
    }

    void getWeather(String lon, String lan, @NonNull Listener callback);

    void getWeatherforFiveDays(String lon, String lan, @NonNull Listener callback);
}
