package com.weatherapp.saugatligal.saugatweatherapp.app.concurrent;

import android.os.Handler;
import android.os.Looper;


public class AndroidHandlerRunner implements ThreadQueue.ThreadRunner {
    private final Handler handler;

    public AndroidHandlerRunner(Handler handler) {
        this.handler = handler;
    }

    public void run(Runnable runnable) {
        if(this.handler.getLooper() == Looper.myLooper()) {
            runnable.run();
        } else {
            this.handler.post(runnable);
        }

    }

    public void runDelayed(Runnable runnable, long delayMilliseconds) {
        this.handler.postDelayed(runnable, delayMilliseconds);
    }

    public void clear() {
        this.handler.removeCallbacksAndMessages((Object)null);
    }
}
