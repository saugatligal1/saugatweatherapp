package com.weatherapp.saugatligal.saugatweatherapp.main;

import com.weatherapp.saugatligal.saugatweatherapp.app.net.Weather;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.WeatherFiveDaysResponse;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherResponse;

import java.util.List;

public class Contract {

    interface View {
        void showProgress();

        void hideProgress();

        void showResult(WeatherResponse weather);

        void showfivedaysWeather(List<com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.List> weatherFiveDaysResponse);

        void showError();

     //   void launchDetails(HighSchool school);
    }

    interface Presenter {
        void attach(View view);
        void initializeLocation(String lon, String lan);
        void detach();


    }
}
