package com.weatherapp.saugatligal.saugatweatherapp.presentation.dagger;


import com.weatherapp.saugatligal.saugatweatherapp.main.WeatherActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApiModule.class, AppModule.class})
public interface AppComponent {
    void inject(WeatherActivity activity);
//    void inject(SchoolDetailsActivity activity);
}
