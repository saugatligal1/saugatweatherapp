package com.weatherapp.saugatligal.saugatweatherapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.weatherapp.saugatligal.saugatweatherapp.R;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.RecyclerViewHolder> {


    private java.util.List<List> list;

    public WeatherAdapter(java.util.List<List> list) {
        this.list = list;
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView time;
        TextView temperature;
        TextView pressure;
        ImageView imageView;

        public RecyclerViewHolder(View v) {
            super(v);
            // binding view
            time = v.findViewById(R.id.editText);
            temperature = v.findViewById(R.id.editText2);
            pressure = v.findViewById(R.id.editText3);
            imageView = v.findViewById(R.id.image);


        }

        public void bind(final List list) {

            time.setText(list.getDt_txt());
            temperature.setText(list.getMain().getTemp() + "F");
            pressure.setText(list.getMain().getHumidity()+"% ");
            if(list.getWeather().get(0).getMain().equals("Clear")){
                imageView.setImageResource((R.drawable.sun));
            }else if(list.getWeather().get(0).getMain().equals("Clouds")){
                imageView.setImageResource((R.drawable.cloudy));
            }else if(list.getWeather().get(0).getMain().equals("Rain")){
                imageView.setImageResource((R.drawable.rain));
            }else if(list.getWeather().get(0).getMain().equals("Snow")){
                imageView.setImageResource((R.drawable.snow));
            }


        }
    }


    @Override
    public WeatherAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating row using inflator
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_detail_row, parent, false);

        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WeatherAdapter.RecyclerViewHolder holder, int position) {

        holder.bind(list.get(position));


    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
