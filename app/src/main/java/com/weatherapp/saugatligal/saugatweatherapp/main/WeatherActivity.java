package com.weatherapp.saugatligal.saugatweatherapp.main;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.weatherapp.saugatligal.saugatweatherapp.MainActivity;
import com.weatherapp.saugatligal.saugatweatherapp.R;
import com.weatherapp.saugatligal.saugatweatherapp.adapter.WeatherAdapter;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherFor5DaysModel.List;
import com.weatherapp.saugatligal.saugatweatherapp.app.net.WeatherResponse;
import com.weatherapp.saugatligal.saugatweatherapp.app.utility.Utility;
import com.weatherapp.saugatligal.saugatweatherapp.presentation.App;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class WeatherActivity extends AppCompatActivity  implements Contract.View {

    @Inject
    WeatherPresenter presenter;

    @BindView(R.id.weather_id)
    TextView weatherTv;

    @BindView(R.id.temperature_id)
    TextView temperatureTv;

    @BindView(R.id.pressure_id)
    TextView pressureTv;

    @BindView(R.id.humidity_id)
    TextView humidityTv;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.weather_image_id)
    ImageView imageView;

    WeatherAdapter adapter;
    RecyclerView.LayoutManager mLayoutManager;
    private FusedLocationProviderClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        requestPermission();
        App.get(this).getAppComponent().inject(this);
        // bind the view using butterknife
        ButterKnife.bind(this);
        client = LocationServices.getFusedLocationProviderClient(this);

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        if (ActivityCompat.checkSelfPermission(WeatherActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        /**
         * Just incase google play service is not installed just added dummy value.
         */

        if(client!=null){
            client.getLastLocation().addOnSuccessListener(WeatherActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    if(location!=null){
                        presenter.initializeLocation(location.getLatitude()+"",location.getLongitude()+"");
                    }else{
                        presenter.initializeLocation("35","139");
                    }


                }
            });
        }else{
            presenter.initializeLocation("35","139");

        }


    }


    @Override
    protected void onStart() {
        super.onStart();
        presenter.attach(this );

    }

    @Override
    public void showProgress() {
        Utility.createProgressDialog(this, "LOADING..").show();
    }

    @Override
    public void hideProgress() {
        Utility.dissmisProgress();
    }


    @Override
    public void showResult(WeatherResponse weather) {
        weatherTv.setText(weather.getName());
        temperatureTv.setText(weather.getMain().getTemp() + " F");
        pressureTv.setText(weather.getMain().getPressure() + " P");
        humidityTv.setText(weather.getMain().getHumidity() + "% Humidity");
        if (weather.getMain().equals("Clear")) {
            imageView.setImageResource((R.drawable.sun));
        } else if (weather.getWeather().get(0).getMain().equals("Clouds")) {
            imageView.setImageResource((R.drawable.cloudy));
        } else if (weather.getWeather().get(0).getMain().equals("Rain")) {
            imageView.setImageResource((R.drawable.rain));
        } else if (weather.getWeather().get(0).getMain().equals("Snow")) {
            imageView.setImageResource((R.drawable.snow));
        }


    }

    @Override
    public void showfivedaysWeather(java.util.List<List> list) {
        if (list != null) {
            Toast.makeText(this, "SUCCESS", Toast.LENGTH_LONG).show();
            adapter = new WeatherAdapter(list);
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void showError() {
        Utility.showToast("There is some backend issue. Please try later", this);

    }

    @Override
    protected void onStop() {
        presenter.detach();
        super.onStop();
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);


    }
}
